{* Product - Line view *}

{def $content_size = '8'}
{def $foxycart_api_key = ezini( 'FoxycartSettings', 'ApiKey', 'foxycart.ini' )}
{def $foxycart_url = ezini( 'FoxycartSettings', 'ApiUrl', 'foxycart.ini' )}

{def $prod_code=$node.data_map.product_number.data_text}
{def $prod_name=$node.name|wash()}
{def $prod_price=$node.data_map.price.content.inc_vat_price}

<div class="content-view-line">
    <article class="class-product row">

    {if $node.data_map.image.content}
        <div class="span2">
            <div class="attribute-image">
                {attribute_view_gui href=$node.url_alias|ezurl image_class=productthumbnail attribute=$node.data_map.image}
            </div>
        </div>
        {set $content_size = '6'}
    {/if}

        <div class="span{$content_size}">
            <div class="attribute-header with-product-number">
                <h2>
                    <a href="{$node.url_alias|ezurl( 'no' )}" class="teaser-link" title="{$node.name|wash()}">{$node.name|wash()}</a>
                </h2>
                <div class="product-number">
                    {attribute_view_gui attribute=$node.data_map.product_number}
                </div>
            </div>

            <div class="attribute-short">
               {attribute_view_gui attribute=$node.data_map.short_description}
            </div>

            <form action="{$foxycart_url}" method="post" accept-charset="utf-8"> 
                <fieldset class="row">
                    <div class="span{$content_size|div( '2' )}">
                        {attribute_view_gui attribute=$node.data_map.additional_options}
                    </div>
                    <div class="span{$content_size|div( '2' )}">
                        <div class="item-price">
                            {attribute_view_gui attribute=$node.data_map.price}
                        </div>
                        <div class="item-buying-action form-inline">
                            <label>
                                <span class="hidden">{'Amount'|i18n("design/ezdemo/line/product")}</span>
                                <input class="span1" type="text" name="{foxycart_hmac(array("quantity", "--OPEN--", $prod_code, $foxycart_api_key))}" />
                            </label>
                            <button class="btn btn-warning" type="submit" name="ActionAddToBasket">
                                {'Buy'|i18n("design/ezdemo/line/product")}
                            </button>
                        </div>
                    </div>
                </fieldset>
                <input type="hidden" name="{foxycart_hmac(array("code", $prod_code, $prod_code, $foxycart_api_key))}" value="{$prod_code}" />
                <input type="hidden" name="{foxycart_hmac(array("name", $prod_name, $prod_code, $foxycart_api_key))}" value="{$prod_name}" />
                <input type="hidden" name="{foxycart_hmac(array("price", $prod_price, $prod_code, $foxycart_api_key))}" value="{$prod_price}" />
        </form>
        </div>

   </article>
</div>

{undef $content_size}
