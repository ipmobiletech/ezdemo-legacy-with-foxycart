<article class="selected-products">
    {def $foxycart_api_key = ezini( 'FoxycartSettings', 'ApiKey', 'foxycart.ini' )}
    {def $foxycart_url = ezini( 'FoxycartSettings', 'ApiUrl', 'foxycart.ini' )}

    {def $prod_code=$node.data_map.product_number.data_text}
    {def $prod_name=$node.name|wash()}
    {def $prod_price=$node.data_map.price.content.inc_vat_price}

    <div class="attribute-image">
        {attribute_view_gui href=$node.url_alias|ezurl image_class=productthumbnail attribute=$node.data_map.image}
    </div>
    <div class="product-info">
        <div class="attribute-header">
            <a href="{$node.url_alias|ezurl( 'no' )}" class="teaser-link">
                <h3>{$prod_name}</h3>
            </a>
        </div>
        <ul class="breadcrumb">
            {foreach $node.path as $path_item}
            <li>
                <a href="{$path_item.url_alias|ezurl( 'no' )}">{$path_item.name|wash()}</a>
                {delimiter}
                <span class="divider">»</span>
                {/delimiter}
            </li>
            {/foreach}
        </ul>

        <form action="{$foxycart_url}" method="post" accept-charset="utf-8"> 
            <fieldset>
                <div class="item-price">
                    {$prod_price|l10n( 'currency' )}
                </div>
                <div class="item-buying-action form-inline">
                    <label>
                        <span class="hidden">{'Amount'|i18n("design/ezdemo/block_item/product")}</span>
                        <input type="text" name="{foxycart_hmac(array("quantity", "--OPEN--", $prod_code, $foxycart_api_key))}" />
                    </label>
                    <button class="btn btn-warning" type="submit" name="ActionAddToBasket">
                        {'Buy'|i18n("design/ezdemo/block_item/product")}
                    </button>

                </div>
            </fieldset>
            <input type="hidden" name="{foxycart_hmac(array("code", $prod_code, $prod_code, $foxycart_api_key))}" value="{$prod_code}" />
            <input type="hidden" name="{foxycart_hmac(array("name", $prod_name, $prod_code, $foxycart_api_key))}" value="{$prod_name}" />
            <input type="hidden" name="{foxycart_hmac(array("price", $prod_price, $prod_code, $foxycart_api_key))}" value="{$prod_price}" />
        </form>
    </div>
</article>
