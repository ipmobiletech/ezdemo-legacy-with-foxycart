<?php
//
// ## BEGIN COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
// SOFTWARE NAME:  Template Operator Get Verification needed for Foxycart HMAC Validation 
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of version 2.0  of the GNU General
//  Public License as published by the Free Software Foundation.
//
//  This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of version 2.0 of the GNU General
//  Public License along with this program; if not, write to the Free
//  Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//  MA 02110-1301, USA.
// ## END COPYRIGHT, LICENSE AND WARRANTY NOTICE ##
//


class FoxyCart_TemplateOperator
{
    /*!
      Constructor, does nothing by default.
    */
    function __construct()
    {

    }

    /*!
     \return an array with the template operator name.
    */
    function operatorList()
    {
        return array( 'foxycart_hmac');
    }

    /*!
     \return true to tell the template engine that the parameter list exists per operator type,
             this is needed for operator classes that have multiple operators.
    */
    function namedParameterPerOperator()
    {
        return true;
    }

    /*!
     See eZTemplateOperator::namedParameterList
    */
    function namedParameterList()
    {
        return array( 'foxycart_hmac' => array( 'tuple' => array( 'type' => 'array',
                                                                           'required' => true
                                                                          ))
                    );
    }

    function foxycart_hmac( $var_name, $var_value, $var_code, $api_key ) {
        
        $encodingval = htmlspecialchars($var_code) . htmlspecialchars($var_name) . htmlspecialchars($var_value);
        return $var_name.'||'.hash_hmac('sha256', $encodingval, $api_key).($var_value === "--OPEN--" ? "||open" : "");
    }

    /*!
     Executes the PHP function for the operator cleanup and modifies \a $operatorValue.
    */
    function modify( $tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue, $namedParameters, $placement )
    {
        $tuple = $namedParameters['tuple'];
        
        switch ( $operatorName )
        {
           case 'foxycart_hmac':
            {
                $operatorValue =  $this->foxycart_hmac($tuple[0], $tuple[1], $tuple[2], $tuple[3]);  
            } break;
        }
    }


}
?>
