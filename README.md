This repository contains the mimimal set of files need to quickly demonstrated FoxyCart integration with an installed copy of eZ Publish ezDemo with content.  Here's the file list:

extension/ezdemo/autoloads/eztemplateautoload.php
extension/ezdemo/autoloads/foxycart_hmac.php
extension/ezdemo/design/ezdemo/override/templates/block_item/product.tpl
extension/ezdemo/design/ezdemo/override/templates/full/product.tpl
extension/ezdemo/design/ezdemo/override/templates/line/product.tpl
extension/ezdemo/design/ezdemo/templates/page_header_links.tpl

extension/ezdemo/settings/foxycart.ini.append.php.change-me


Simply copy these files into your ezdemo extension, add your FoxyCart account info into foxycart.ini.append.php.change-me, and rename foxycart.ini.append.php.change-me to foxycart.ini.append.php then follow by performing 'php bin/php/ezpgenerateautoloads.php -e' to add the template operator.


Since this demo is using FoxyCart HMAC Validation, you'll need to enable the "would you like to enable cart validation?" checkbox in the Advanced Settings in FoxyCart Admin panel. Should you not enable the validation, item won't get added to the cart.
